# Musicwhore.org Archive Theme

A custom theme for the [Musicwhore.org Archive](http://archive.musicwhore.org/).

## Dependencies

* [Musicwhore.org 2015 Theme](https://bitbucket.org/NemesisVex/musicwhore.org-2015-theme-for-wordpress)

## Implementations

### Musicwhore.org Artist Connector

The `TemplateTags` class queries the Musicwhore.org Artist Database to display artist content.

The `page-templates` directory contain custom templates for artist content.

The `ArtistList` widget creates a sidebar widget to browse the artist directory by initial alphabet.

See the [plugin documentation](https://bitbucket.org/NemesisVex/musicwhore-artist-connector-for-wordpress)
for usage.
